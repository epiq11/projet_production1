-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 05 juil. 2020 à 23:57
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projetprod1`
--

-- --------------------------------------------------------

--
-- Structure de la table `blog`
--

DROP TABLE IF EXISTS `blog`;
CREATE TABLE IF NOT EXISTS `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(50) DEFAULT NULL,
  `article` text DEFAULT NULL,
  `date_creation` datetime DEFAULT current_timestamp(),
  `id_utilisateur` int(11) NOT NULL,
  PRIMARY KEY (`id`,`id_utilisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `blog`
--

INSERT INTO `blog` (`id`, `titre`, `article`, `date_creation`, `id_utilisateur`) VALUES
(12, 'Allo Prof123', 'test tts', '2020-07-05 15:35:31', 2),
(13, 'test retour de ligne', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ornare eget massa sed posuere. Sed diam neque, scelerisque vitae varius a, vestibulum in purus. Fusce ut magna sollicitudin, pellentesque diam nec, tempus enim. In hac habitasse platea dictumst. In hac habitasse platea dictumst. Phasellus id feugiat elit. Aenean in dolor dapibus, cursus odio et, tincidunt libero. Nulla erat neque, pellentesque vitae lacus nec, efficitur bibendum neque. Proin fringilla vel lectus eget maximus. Aenean in turpis vel magna sagittis facilisis. Duis et posuere nisl. Curabitur ut velit cursus, tempus elit maximus, luctus mauris. Pellentesque vulputate velit et tincidunt scelerisque. Cras vitae purus ultrices nunc ullamcorper porttitor. Nam tempus porttitor dolor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.\r\n\r\nIn suscipit ante eget leo sagittis mollis. Ut nunc elit, egestas vulputate metus sit amet, blandit semper nisl. Nam id nulla quis mi sagittis sagittis et non nisl. Duis eget ante a tellus rutrum tempus. Quisque rutrum sapien ac mi facilisis ornare. Aenean magna turpis, congue sed vulputate quis, tincidunt nec velit. Ut cursus lacinia tellus, ac elementum dolor hendrerit eu. Donec tristique odio id sapien condimentum, nec vehicula tellus vestibulum. Phasellus ut varius massa. Ut sit amet scelerisque velit. Nullam luctus eleifend dolor, ac euismod ex tristique id. Proin mi ipsum, consectetur id nulla id, mattis tempor nunc. Sed et est in ante consectetur ornare nec at tortor. Praesent erat justo, ultricies lobortis pretium non, fermentum quis metus.', '2020-07-05 15:42:29', 2),
(14, 'Text alÃ©atoire', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In suscipit ante eget leo sagittis mollis. Ut nunc elit, egestas vulputate metus sit amet, blandit semper nisl. Nam id nulla quis mi sagittis sagittis et non nisl. Duis eget ante a tellus rutrum tempus. Quisque rutrum sapien ac mi facilisis ornare. Aenean magna turpis, congue sed vulputate quis, tincidunt nec velit. Ut cursus lacinia tellus, ac elementum dolor hendrerit eu. Donec tristique odio id sapien condimentum, nec vehicula tellus vestibulum. Phasellus ut varius massa. Ut sit amet scelerisque velit. Nullam luctus eleifend dolor, ac euismod ex tristique id. Proin mi ipsum, consectetur id nulla id, mattis tempor nunc. Sed et est in ante consectetur ornare nec at tortor. Praesent erat justo, ultricies lobortis pretium non, fermentum quis metus.', '2020-07-05 17:28:38', 2);

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) NOT NULL,
  `date_livrer` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`id`, `nom`, `date_livrer`) VALUES
(1, 'base', '2022-07-30'),
(2, 'pas base', '2022-12-16'),
(3, 'gros gros luxe', '2023-11-25');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `mot_de_passe` varchar(255) DEFAULT NULL,
  `courriel` varchar(100) NOT NULL,
  `id_produit` int(11) DEFAULT NULL,
  `est_admin` smallint(6) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `nom`, `prenom`, `mot_de_passe`, `courriel`, `id_produit`, `est_admin`) VALUES
(2, 'Bergeron', 'Eric', '$2y$10$PQuDJ.feqPJ0.FEtdpyMF.K34wBSqwx6CSWn74rbnB3plOZY.qIrG', 'epiq11@gmail.com', 3, 1),
(4, 'Bergeron', 'Eric', '$2y$10$X5x9iXmVJjbyR4eDRdI8kOFxsx1qXFOOpQEdRwk8Zv70w9yluYO0G', 'eb@gmail.com', 3, 0),
(27, 'Berger', 'Allo', '$2y$10$7Q7Cjm0nB.y1MdML37hLEu6OD54XsmvG4KbSQWwbbkuywMfrP2bay', 'allo@gmail.com', 1, 0),
(28, 'Bebe', 'Ok', '$2y$10$XpS1/zBHBXZod2BFWzzKF.sfnKeQrMAdd3An7CaQNSJ16ahtYibCa', 'ok@g.com', 1, 0),
(29, NULL, NULL, NULL, 'test@allo.com', NULL, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
