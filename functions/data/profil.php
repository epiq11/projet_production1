<?php
        
function selection_profil($courriel = ""){
    $conn = connect();
    try{
        $requete = $conn->prepare("SELECT * FROM utilisateur Where courriel = :courriel");
        $requete->execute([":courriel"=>$courriel]);
        return $requete;
    } catch(PDOException $e){
        echo 'Erreur : ' . $e->getMessage();
    }
    $conn = null;
    Exit();
}

function selection_user_And_produit($courriel = ""){
    $conn = connect();

    try{
        $requete = $conn->prepare("SELECT *, produit.nom AS nom_produit, utilisateur.nom AS nom, utilisateur.id AS id
                                    FROM utilisateur
                                    INNER JOIN produit
                                    ON utilisateur.id_produit = produit.id
                                    WHERE utilisateur.courriel = :courriel");
        $requete->execute([":courriel"=>$courriel]);
        return $requete;
    } catch(PDOException $e){
        echo 'Erreur : ' . $e->getMessage();
    }
    $conn = null;
    Exit();
}

function verif_email($courriel = ""){
    $conn = connect();
    try{
        $requete = $conn->prepare("SELECT courriel
                                    FROM utilisateur
                                    WHERE courriel = :courriel");
        $requete->execute([":courriel"=>$courriel]);

        $email_count = $requete->rowCount();
        return $email_count;
    } catch(PDOException $e){
        echo 'Erreur : ' . $e->getMessage();
    }
    $conn = null;
    Exit();
}

function verif_mot_de_passe($mdp = "", $email ){
    $conn = connect();
    $resultat = null;

    $result_select_profil = selection_user_And_produit($email)->fetch();

    if (password_verify($mdp, $result_select_profil["mot_de_passe"])){
         $resultat["id"] = $result_select_profil["id"];
         $resultat["prenom"] = $result_select_profil["prenom"];
         $resultat["nom"] = $result_select_profil["nom"];
         $resultat["courriel"] = $result_select_profil["courriel"];
         $resultat["id_produit"] = $result_select_profil["id_produit"];
         $resultat["est_admin"] = $result_select_profil["est_admin"];
         $resultat["nom_produit"] = $result_select_profil["nom_produit"];
         $resultat["date_livrer"] = $result_select_profil["date_livrer"];
    }
    else{
         $resultat = 0;
    }

return $resultat;

}

function login($mot_de_passe, $email){

    $resultat = null;
    $infoConnecter = "";
    $resultat = verif_mot_de_passe($mot_de_passe, $email );

    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {

        if ( isset($resultat["id"]) ){
            $_SESSION["courriel"] = $resultat["courriel"];
            $_SESSION["nom"] = $resultat["nom"];
            $_SESSION["prenom"] = $resultat["prenom"];
            $_SESSION["id"] = $resultat["id"];
            $_SESSION["id_produit"] = $resultat["id_produit"];
            $_SESSION["est_admin"] = $resultat["est_admin"];
            $_SESSION["nom_produit"] = $resultat["nom_produit"];
            $_SESSION["date_livrer"] = $resultat["date_livrer"];
            header("Refresh:0");
        }
        else
        {
            $infoConnecter = '<span class="red">Information de connection incorrecte.</span>';
        }

        return $infoConnecter;
    }
            
}

function user_update($utilisateur){

    $utilisateur["id"] = (isset($utilisateur["id"]) ? $utilisateur["id"] : "");
    $utilisateur["nom"] = (isset($utilisateur["nom"]) ? $utilisateur["nom"] : "");
    $utilisateur["prenom"] = (isset($utilisateur["prenom"]) ? $utilisateur["prenom"] : "");
    $utilisateur["mot_de_passe"] = (isset($utilisateur["mot_de_passe"]) ? $utilisateur["mot_de_passe"] : "");
    $utilisateur["id_produit"] = (isset($utilisateur["id_produit"]) ? $utilisateur["id_produit"] : "");
    $utilisateur["courriel"] = (isset($utilisateur["courriel"]) ? $utilisateur["courriel"] : "");


    $errors = validate_form($utilisateur["mot_de_passe"]);
    $errors = (isset($errors)) ? $errors : "";
    $errors_count = -1;

    if (is_array($errors)){
        $errors_count = count($errors);
        foreach ($errors as $key => $value){
            if (empty($value)){
                $errors_count = $errors_count - 1;
            }
        }
    }

    if($errors_count == 0 && $_SERVER["REQUEST_METHOD"] == "POST"){ 

        $conn = connect();

        try{

            $pdo = $conn->prepare("UPDATE utilisateur SET nom = :nom, prenom = :prenom, mot_de_passe = :mot_de_passe, id_produit = :id_produit WHERE id = :id"); 
              
            $pdo->bindParam(':nom',$utilisateur["nom"]);
            $pdo->bindParam(':prenom',$utilisateur["prenom"]);

            if (!empty($utilisateur["nom"])) {
                $pdo->bindParam(':mot_de_passe',$utilisateur["mot_de_passe"]);
            }

            $pdo->bindParam(':id_produit',$utilisateur["id_produit"]);
            $pdo->bindParam(':id',$utilisateur["id"]);

            $pdo->execute(); 
            return  '<span class="span textGreen justifyCenter">Modification inscrit dans la base de donnée.</span> <br/>';
        }
        catch(PDOException $e){
            return "Erreur : " . $e->getMessage();
            $conn->rollBack();
        }
        $conn = null;
    }
    else{
        return '<span class="red justifyCenter"> Veillez remplir tout le formulaire.</span> <br/>';;
    }
}

