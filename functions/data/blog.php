<?php
function compte_nb_article(){
    
    $conn = connect();
    try{
        $requete = $conn->query("SELECT COUNT(id) FROM blog");
        $requete = $requete->fetch();
        return $requete["COUNT(id)"];
    } catch(PDOException $e){
        echo 'Erreur : ' . $e->getMessage();
    }
    $conn = null;
    Exit();
}

function supprimer_article(){

    $supprimer_article = (isset($_GET["supprimer"]) ? $_GET["supprimer"] : "");
        
    $conn = connect();

    if ( isset($_GET["supprimer"]) ) {
        try{
            $pdo = $conn->prepare("DELETE FROM blog WHERE id = :id");

            $pdo->bindParam(':id',$supprimer_article);
            $pdo->execute();

            return  '<span class="span"><?php Article supprimé du Blog.></span>';

        }catch(PDOException $e){
            return "Erreur : " . $e->getMessage();
            $conn->rollBack();
        }
        $conn = null;
    }
}

function blog_afficher($id){
    $conn = connect();
    try{
        $requete = $conn->prepare("SELECT * FROM blog Where id = :id");
        $requete->execute([":id"=>$id]);
        return $requete;
    } catch(PDOException $e){
        echo 'Erreur : ' . $e->getMessage();
    }
    $conn = null;
    Exit();
}

function liste_article($limit = null){

    $query_count = compte_nb_article();

    if ($limit == null) {
        $limit = "0,$query_count";
    }else {
        $limit = $limit;
    }

    $conn = connect();
    try{
        $requete = $conn->query("SELECT blog.id , blog.titre, blog.article, blog.date_creation, blog.id_utilisateur,
                                        utilisateur.nom AS utilisateur_nom, utilisateur.prenom AS utilisateur_prenom
                                 FROM blog
                                 INNER JOIN utilisateur
                                    ON blog.id_utilisateur = utilisateur.id
                                    
                                 ORDER BY Blog.date_creation DESC
                                 LIMIT $limit");
        return $requete;
    } catch(PDOException $e){
        echo 'Erreur : ' . $e->getMessage();
    }
    $conn = null;
    Exit();
}

function recheche_un_article($id){
    $conn = connect();
    try{
        $requete = $conn->prepare("SELECT blog.id , blog.titre, blog.article, blog.date_creation, blog.id_utilisateur,
                                        utilisateur.nom AS utilisateur_nom, utilisateur.prenom AS utilisateur_prenom
                                 FROM blog
                                 INNER JOIN utilisateur
                                    ON blog.id_utilisateur = utilisateur.id
                                    
                                 Where blog.id = :id");
        $requete->execute([":id"=>$id]);
        return $requete;
    } catch(PDOException $e){
        echo 'Erreur : ' . $e->getMessage();
    }
    $conn = null;
    Exit();
}

function nouveau_article(){
    
    $titre = (isset($_POST["titre"]) ? $_POST["titre"] : "");
    $texte = (isset($_POST["texte"]) ? $_POST["texte"] : "");
    $id = $_SESSION["id"];

    $errors_count = -1;
    $errors = validate_form();
    $errors = (isset($errors)) ? validate_form() : "";


    if (is_array($errors)){
        $errors_count = count($errors);
        foreach ($errors as $key => $value){
            if (empty($value)){
                $errors_count = $errors_count - 1;
            }
        }
    }


    if($errors_count == 0){

        $conn = connect();

        if (isset($_POST["boutonNouveauArticle"])) {

            try{
                $pdo = $conn->prepare("INSERT INTO blog (titre, article, id_utilisateur ) 
                                    VALUES (:titre,:article,:id_utilisateur)"); 

                $pdo->bindParam(':titre',$titre);
                $pdo->bindParam(':article',$texte);
                $pdo->bindParam(':id_utilisateur',$id);

                $pdo->execute([$titre,$texte,$id]); 

                return  '<span class="span textGreen justifyCenter">Article publier.</span> <br/>';
            }catch(PDOException $e){
                return "Erreur : " . $e->getMessage();
                $conn->rollBack();
            }
            $conn = null;
        }
    }
}


function blog_update(){

    $titre = (isset($_POST["titre"]) ? $_POST["titre"] : "");
    $texte = (isset($_POST["texte"]) ? $_POST["texte"] : "");
    $id_blog = (isset($_SESSION["id_blog"]) ? $_SESSION["id_blog"] : "");


    $errors = validate_form();
    $errors = (isset($errors)) ? validate_form() : "";
    $errors_count = -1;
    

    if (is_array($errors)){
        $errors_count = count($errors);
        foreach ($errors as $key => $value){
            if (empty($value)){
                $errors_count = $errors_count - 1;
            }
        }
    }

    if($errors_count == 0){ 

        $conn = connect();

        if ( isset($_POST["boutonNouveauArticle"]) ) {

            try{
                $pdo = $conn->prepare("UPDATE blog SET titre = :titre, article = :article WHERE id = :id"); 
              
                $pdo->bindParam(':titre',$titre);
                $pdo->bindParam(':article',$texte);
                $pdo->bindParam(':id',$id_blog);

                $pdo->execute([$titre,$texte,$id_blog]);  
                return  '<span class="span textGreen
                 justifyCenter">Modification inscrit dans la base de donnée.</span> <br/>';
            }catch(PDOException $e){
                return "Erreur : " . $e->getMessage();
                $conn->rollBack();
            }
            $conn = null;
        }
    }
}