<?php

function liste_produit(){
    $conn = connect();
    try{
        $requete = $conn->query("SELECT * FROM produit ORDER BY id DESC");
        return $requete;
    } catch(PDOException $e){
        echo 'Erreur : ' . $e->getMessage();
    }
    $conn = null;
    Exit();
}