<?php
$errors = null;

function form_values($nom){
    return (isset($nom) ? $nom : "");
}

function validate_text_fields($text){
    return trim(htmlspecialchars($text));
}

function deconnexion($name_bouton){
    if (isset($name_bouton)){
        session_destroy();
        header("Refresh:0");
    }
}

function validate_form($email = ""){
        $errors = [];

        if (isset($_POST["boutonCommandez"])){

            $query = selection_profil($email)->fetch();

            if ( $query == false && $query["mot_de_passe"] == null ) {

                $errors["courriel"] = (empty(validate_email($_POST["courriel"]))) ? "Inscrire un courriel et il doit être formatez correctement." : "";
                $errors["mot_de_passe"] = (empty($_POST["mot_de_passe"])) ? "Inscrire un mot de passe." : "";
                $errors["prenom"] = (empty($_POST["prenom"])) ? "Inscrire un prénom." : "";
                $errors["nom"] = (empty($_POST["nom"])) ? "Inscrire un nom." : "";
                $errors["produit"] = (empty($_POST["id_produit"])) ? "Choisir un produit." : "";

            }elseif ( is_array($query) && $query["mot_de_passe"] == null  ) {

                $errors["mot_de_passe"] = (empty($_POST["mot_de_passe"])) ? "Inscrire votre mot de passe." : "";
                $errors["courriel"] = "";
                $errors["prenom"] = "";
                $errors["nom"] = "";
                $errors["produit"] = "";

            }elseif ( is_array($query) && $_POST["mot_de_passe"] == ""  ) {

                $errors["mot_de_passe"] = (empty($_POST["mot_de_passe"])) ? "Inscrire votre mot de passe." : "";
                $errors["courriel"] = "";
                $errors["prenom"] = "";
                $errors["nom"] = "";
                $errors["produit"] = "";
            }
        }

        if (isset($_POST["boutonNouveauArticle"])){
            $errors["titre"] = (empty($_POST["titre"])) ? "Ajouter un Titre." : "";
            $errors["texte"] = (empty($_POST["texte"])) ? "Ajouter du texte." : "";
        }

        if (count($errors) >= 1 ) {
            return $errors;
        }
}

function validate_email($courriel){
    return filter_var($courriel, FILTER_VALIDATE_EMAIL);
}

function inscriptionInfolettre(){

    $utilisateur["courriel"] = (isset($_POST["courrielInfolettre"]) ? $_POST["courrielInfolettre"] : "");

    $query = verif_email($utilisateur["courriel"]);
    $query = (isset($query) ? $query : "");

    if ( $query == 0 ){

        $conn = connect();

        try{
            $pdo = $conn->prepare("INSERT INTO utilisateur (courriel)
                                    VALUES (:courriel)"); 
            $pdo->bindValue(':courriel',$utilisateur["courriel"]);
            $pdo->execute(); 
            return  '<span class="textGreen justifyCenter">Votre email est enregistré.</span> <br/>';
        }catch(PDOException $e){
            return '<span class="red justifyCenter"> Erreur : ' . $e->getMessage() . '</span> <br/>';
            $conn->rollBack();
        }
            $conn = null;
    }else {
        return  '<span class="red justifyCenter"><a href="#infolettre">Entrez une autre adresse email.</a></span> <br/>';
    }

}

function inscriptionLogin(){

    $mot_de_passe_post = (isset($_POST["mot_de_passe"]) ? $_POST["mot_de_passe"] : "");
    
    $utilisateur["nom"] = (isset($_POST["nom"]) ? $_POST["nom"] : "");
    $utilisateur["prenom"] = (isset($_POST["prenom"]) ? $_POST["prenom"] : "");
    $utilisateur["mot_de_passe"] = (isset($mot_de_passe_post) ? password_hash($mot_de_passe_post, PASSWORD_DEFAULT) : "");
    $utilisateur["id_produit"] = (isset($_POST["id_produit"]) ? $_POST["id_produit"] : "");
    $utilisateur["courriel"] = (isset($_POST["courriel"]) ? $_POST["courriel"] : "");

    $query_verif_email = verif_email($utilisateur["courriel"]);
    $query_verif_email = (isset($query_verif_email) ? $query_verif_email : "");

    $query = selection_profil($utilisateur["courriel"])->fetch();
    $utilisateur["id"] = (isset($query["id"]) ? $query["id"] : "");

    $errors = validate_form($utilisateur["courriel"]);
    $errors = (isset($errors)) ? $errors : "";
    $errors_count = 0;


    if (is_array($errors)){
        $errors_count = count($errors);
        foreach ($errors as $key => $value){
            if (empty($value)){
                $errors_count = $errors_count - 1;
            }
        }
    }

    if($errors_count == 0){

        $conn = connect();

        if ( $query_verif_email == 0 && $query["mot_de_passe"] == null ) {
            
            if ($utilisateur["courriel"] != null && $utilisateur["courriel"] != "") {
                try{
                    $pdo = $conn->prepare("INSERT INTO utilisateur (nom, prenom, mot_de_passe, courriel ,id_produit ) 
                                        VALUES (:nom,:prenom,:mot_de_passe,:courriel,:id_produit)"); 
    
                    $pdo->bindParam(':nom',$utilisateur["nom"]);
                    $pdo->bindParam(':prenom',$utilisateur["prenom"]);
                    $pdo->bindParam(':mot_de_passe',$utilisateur["mot_de_passe"]);
                    $pdo->bindParam(':courriel',$utilisateur["courriel"]);
                    $pdo->bindParam(':id_produit',$utilisateur["id_produit"]);
    
                    $pdo->execute([$utilisateur["nom"],$utilisateur["prenom"],$utilisateur["mot_de_passe"],$utilisateur["courriel"],$utilisateur["id_produit"]]); 
    
                    return  '<span class="textGreen justifyCenter">Votre compte est maintenant créé.</span> <br/>';
                }catch(PDOException $e){
                    return '<span class="red justifyCenter"> Erreur : ' . $e->getMessage() . '</span> <br/>';
                    $conn->rollBack();
                }
                $conn = null;
            }

        }
        elseif ( $query_verif_email == 1 && $query["mot_de_passe"] == null ) 
        {
            return user_update($utilisateur);
              
        }elseif ( $query_verif_email == 1 && $query["mot_de_passe"] != null) {

            login($mot_de_passe_post, $utilisateur["courriel"]);
            
        }
    }

}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
    $email = (isset($_POST["courriel"]) ? $_POST["courriel"] : "");
    $form = validate_form($email);

    if(is_array($form)){
        if (count($form) > 0 ) {
            $errors = $form;
        }
    }

    return $errors;
}

?>

	
