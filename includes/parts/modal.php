<?php
    if (isset($_SESSION["id"]) != "" && isset($_SESSION["id"]) != null){
        $titre = 'Votre profil, ' . ucfirst($_SESSION['prenom']) . ' ' . ucfirst($_SESSION['nom']) ;
    }else {
        $titre = "Inscription/Connexion";
    }

?>

<!-- Modal -->
<div class="modal fade" id="activeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true"> <!-- Modal -->
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?php echo $titre; ?></h5>
            </div>

<?php

$errors_mot_de_passe = form_values($errors["mot_de_passe"]);
$errors_courriel = form_values($errors["courriel"]);
$errors_prenom = form_values($errors["prenom"]);
$errors_nom = form_values($errors["nom"]);
$errors_produit = form_values($errors["produit"]);

?>
            <div class="modal-body">
                    <form action="/" method="POST" id="formulaire" class="form" name="formulaire">

                    <?php
                    if (isset($_SESSION["id"])){
                        echo '<div class="text-center mb-4"><a href="blog_admin.php">ADMINISTRATION DU BLOG</a></div>';
                        echo "<p class='mb-3'> Produit Commandé :  " . $_SESSION["nom_produit"] . "  </p>";
                        echo "<p> Date de réception de votre produit :  " . date('d-m-Y', strtotime($_SESSION["date_livrer"])) . "</p>";
                    }else {
                    
                    ?>

                        <div class="form-group">
                            <label for="courriel" class="col-form-label">Courriel:</label>
                            <input type="text" id="courriel" class="form-control" name="courriel"
                                placeholder="xxx@xxxx.xxx">
                            <span class="red"><?php echo $errors_courriel ?></span>
                        </div>
                        <div class="form-group">
                            <label for="mot_de_passe" class="col-form-label">Mot de passe:</label>
                            <input type="password" id="mot_de_passe" class="form-control" name="mot_de_passe"
                                placeholder="Mot de passe">
                            <span class="red"><?php echo $errors_mot_de_passe ?></span>
                            
                        </div>
                        
                        <p class="mb-0">Remplir aussi ci-dessous pour l'inscription:</p>

                        <div class="form-group border-top border-gray pt-4">
                            <label for="prenom" class="col-form-label">Prénom:</label>
                            <input type="text" id="prenom" class="form-control" name="prenom" placeholder="Prénom">
                            <span class="red"><?php echo $errors_prenom ?></span>
                        </div>
                        <div class="form-group">
                            <label for="nom" class="col-form-label">Nom:</label>
                            <input type="text" id="nom" class="form-control" name="nom" placeholder="Nom de famille">
                            <span class="red"><?php echo $errors_nom ?></span>
                        </div>
                        <fieldset class="form-group ">
                            <div class="row">
                                <legend class="col-form-label col-sm-5 pt-0">Choisir votre version:</legend>
                                <div class="col-sm-7">

                                    <?php 
                                        $query = liste_produit()->fetchAll();

                                        foreach ($query as $key) {
                                            $key_id = $key["id"];
                                            $key_nom = ucfirst($key["nom"]);
                                            $checked = ($key_id == 3 ? "checked='checked'" : "");

                                            echo "<div class='form-check'>";
                                                echo "<input id='produit$key_id' class='form-check-input' type='radio' name='id_produit' value='$key_id' $checked />";
                                                echo "<label class='form-check-label' for='produit$key_id'>";
                                                    echo $key_nom;
                                                echo "</label>";
                                            echo "</div>";


                                        }
                                    
                                    ?>
                                    <span class="red"><?php echo validate_text_fields($errors_produit) ?></span>
                                </div>
                            </div>
                        </fieldset>
                    <?php
                    }
                    ?>
            </div>

            <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    

                    <?php
                    if (isset($_SESSION["id"])) {
                    ?>
                        <input type="submit" class="btn btn-primary" id="button2" name="boutonDeconnecter" value="Se déconnecter">
                    <?php  
                    }else {
                    ?>
                        <input type="submit" class="btn btn-primary" id="button2" name="boutonCommandez" value="Soumettre">
                    <?php       
                    }
                    ?>
                </form>
            </div>
        </div>
    </div>
</div>