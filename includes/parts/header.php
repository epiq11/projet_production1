<?php
session_start();

?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">

    <title>Le Sub-discombobulateur Atomique</title>
</head>


<body>
    <header>
        <div class="navbar-dark bg-dark box-shadow">
            <div class="container d-flex justify-content-between py-3">
                <div>
                    <a href="/">
                        <svg id="logo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1011.1 408.7">
                            <g id="Calque_2" data-name="Calque 2">
                                <g id="Calque_1-2" data-name="Calque 1">
                                    <g>
                                        <polyline class="cls-1" points="108.4 402.5 340.4 12 688 12 934.4 395.5" />
                                        <line class="cls-1" x1="523.3" y1="12" x2="523.3" y2="402.5" />
                                        <g>
                                            <line class="cls-1" x1="341" y1="12" x2="329" y2="12" />
                                            <line class="cls-2" x1="304.6" y1="12" x2="24.2" y2="12" />
                                            <line class="cls-1" x1="12" y1="12" y2="12" />
                                        </g>
                                        <g>
                                            <line class="cls-1" x1="1011.1" y1="12" x2="999.1" y2="12" />
                                            <line class="cls-2" x1="974.7" y1="12" x2="694.3" y2="12" />
                                            <line class="cls-1" x1="682.1" y1="12" x2="670.1" y2="12" />
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </a>
                </div>
                <div>

                    <?php
                    if (isset($_SESSION["id"])) {
                    ?>
                        <button type="button" id="anim_bouton_modal" class="btn btn-primary" data-toggle="modal" data-target="#activeModal">
                            Votre Profil
                        </button>
                    <?php  
                    }else {
                    ?>
                        <button type="button" id="anim_bouton_modal" class="btn btn-primary" data-toggle="modal" data-target="#activeModal">
                            Inscription/Connexion
                        </button>
                    <?php 
                            
                    }
                    ?>

                </div>
            </div>
        </div>

    </header>