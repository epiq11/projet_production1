<?php 
include_once '././functions/functions.php';
include_once './functions/data/connecteur.php';
include_once './functions/data/profil.php';
include_once './functions/data/blog.php';
include_once '././functions/data/produit.php';
include_once './functions/form-functions.php';
include_once './functions/data/profil.php';
include_once './includes/parts/header.php';

$inscriptionLogin_return = "";
$inscriptionInfolettre_return = "";
$errors_formulaire = null;
$errors_count = -1;

if (isset($_POST["boutonInfolettre"])) {
    $inscriptionInfolettre_return = inscriptionInfolettre();
}else {
    $inscriptionLogin_return = inscriptionLogin();
}


if (isset($_POST["boutonDeconnecter"])){
    deconnexion($_POST["boutonDeconnecter"]);
}

if (is_array($errors)){
    $errors_count = count($errors);
    foreach ($errors as $key => $value){
        if (empty($value)){
            $errors_count = $errors_count - 1;
        }
    }
}

if ($errors_count >= 1) {
    $errors_formulaire = '<span class="red justifyCenter"> Entrée du formulaire inconrect.</span> <br/>';
}

?>

<section>
    <div class="container border-bottom border-gray">
        <div class="text-center">
            <?php 
                echo $inscriptionLogin_return;
                echo $inscriptionInfolettre_return;
                echo $errors_formulaire;
            ?>
            <h1 class="mt-3 mb-3">Le Sub-discombobulateur Atomique</h1>
        </div>

        <div class="row">
            <div class="col-lg-4 d-flex align-items-stretch">
                <div id="anim_produit" class="card mb-4">
                    <img class="card-img-top img-fluid" src="img/base16_9.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Version de base (1250,99$)</h5>
                        <p class="card-text">
                        <ul>
                            <li>
                                Bonne pour deux utilisations.
                            </li>
                            <li>
                                Elle attire 50 % des molécule atomique 15 pied autour de l'utilisateur.
                            </li>
                            <li>
                                Pour plus de sécurité elle est impossible à modifier. Si la boite est ouverte elle
                                explose.
                            </li>
                            <li>
                                Elle est blanche (tout le monde sait que la couleur ça coute cher)
                            </li>
                        </ul>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 d-flex align-items-stretch">
                <div id="anim_produit" class="card mb-4">
                    <img class="card-img-top img-fluid" src="img/pabase16_9.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Version de pas base (3050,74$)</h5>
                        <p class="card-text">
                        <ul>
                            <li>
                                Bonne pour deux utilisations.
                            </li>
                            <li>
                                Elle attire 25 % des molécule atomique 10 pied autour de l'utilisateur.
                            </li>
                            <li>
                                Pour plus de sécurité elle est trempée dans un plastique qui la rend monobloc et
                                impénétrable.
                            </li>
                            <li>
                                Elle est de couleur pastel
                            </li>
                        </ul>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-4 d-flex align-items-stretch">
                <div id="anim_produit" class="card mb-4">
                    <img class="card-img-top img-fluid" src="img/paspasbase16_9.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Version de gros gros luxe (10287,23$)</h5>
                        <p class="card-text">
                        <ul>
                            <li>
                                Bonne pour 2 utilisations
                            </li>
                            <li>
                                Elle n'attire aucune molécule nocive.
                            </li>
                            <li>
                                Pour plus de sécurité elle est trempée dans un plastique qui la rend
                                monobloc et impénétrable qui elle est recouverte d’une couche d’acier
                                pour plus d’impénétrabilité.
                            </li>
                            <li>
                                De plus une couverture de fonte permet de ne pas le perdre car au poids
                                qu'il a vous ne pouvez l'oublier! (Plus c'est lourd plus ça vaut cher non?).
                            </li>
                            <li>
                                Elle a deux choix de couleurs vive (tout le monde sait que la couleur ça
                                coute cher) Rouge ou brun vif.
                            </li>
                        </ul>
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section>
    <div class="container text-center">
        <h2 class="mt-3 mb-3">Blog</h2>
    </div>
    <div class="container border-bottom border-gray">

        <div class="row d-flex justify-content-center">
            <?php
                $query = liste_article(2)->fetchAll();
                
                foreach ($query as $row) {
                    $id = $row["id"];
                    $date_creation = $row["date_creation"];
                    $titre = $row["titre"];
                    $texte = $row["article"];
                    $utilisateur_nom = $row["utilisateur_nom"];
                    $utilisateur_prenom = $row["utilisateur_prenom"];
            ?> 
                    <div class="col-lg-4">
                        <div class="card mb-4">
                            <div class="card-header">
                                <h5 class="card-title"><?php echo validate_text_fields($titre); ?></h5>
                            </div>
                            <div class="card-body">
                                <p class="card-text">
                                    <?php echo substr(validate_text_fields($texte), 0, 200) . " ..." ?>
                                </p>
                            </div>
                            <div class="card-footer text-muted">
                                <p>Créer par <strong><?php echo validate_text_fields(ucfirst($utilisateur_prenom)) . " " . validate_text_fields(ucfirst($utilisateur_nom)) . "</strong> le " . date('d-m-Y H:i:s', strtotime($date_creation)) ; ?></p>
                            </div>
                        </div>
                    </div>
            <?php
                }
            ?>

        </div>

        <div class="container text-center mb-2">
            <a href="/blog.php" class="btn btn-primary">Voir tous les articles</a>
        </div>
    </div>
</section>

<section>
    <div id="infolettre" class="container text-center">
        <h2 class="mt-3 mb-3">Abonnez-vous à l’infolettre</h2>
    </div>
    <div class="container">

        <div class="row d-flex justify-content-center">
            <form action="/" method="POST" id="formulaire" class="form-inline" name="formulaire">
                <div class="form-group mx-sm-3 mb-2">
                    <label for="email">Email:</label>
                    <input type="email" name="courrielInfolettre" class="form-control ml-3" id="email" placeholder="email@example.com">
                </div>
                <input type="submit" class="btn btn-primary mb-2" name="boutonInfolettre" value="Soumettre">
            </form>
        </div>

    </div>
</section>

<?php 
    include_once './includes/parts/modal.php';
?>

<?php 
    include_once './includes/parts/footer.php';
?>