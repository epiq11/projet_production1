<?php 
include_once '././functions/functions.php';
include_once './functions/data/connecteur.php';
include_once './functions/data/profil.php';
include_once './functions/data/blog.php';
include_once '././functions/data/produit.php';
include_once './functions/form-functions.php';
include_once './functions/data/profil.php';
include_once './includes/parts/header.php';

?>
    <section>
        <div class="container text-center">
            <h2 class="mt-3 mb-3">Blog</h2>
        </div>
        <div class="container">

            <div class="row d-flex justify-content-center">

                <?php 
                    $query = liste_article()->fetchAll();
                            
                    foreach ($query as $row) {
                    $id = $row["id"];
                    $date_creation = $row["date_creation"];
                    $titre = $row["titre"];
                    $texte = $row["article"];
                    $utilisateur_nom = $row["utilisateur_nom"];
                    $utilisateur_prenom = $row["utilisateur_prenom"];

                ?>
                <div class="col-md-12">
                    <div class="card mb-4">
                        <div class="card-header">
                            <h5 class="card-title"><?php echo validate_text_fields($titre); ?></h5>
                        </div>
                        <div class="card-body">
                            <p class="card-text"><?php echo validate_text_fields($texte); ?></p>
                        </div>
                        <div class="card-footer text-muted">
                        <p>Créer par <strong><?php echo validate_text_fields(ucfirst($utilisateur_prenom)) . " " . validate_text_fields(ucfirst($utilisateur_nom)) . "</strong> le " . date('d-m-Y H:i:s', strtotime($date_creation)) ; ?></p>
                        </div>
                    </div>
                </div>
                <?php 
                    }
                ?>

            </div>
        </div>
    </section>

<?php 
    include_once './includes/parts/modal.php';
?>

<?php 
    include_once './includes/parts/footer.php';
?>